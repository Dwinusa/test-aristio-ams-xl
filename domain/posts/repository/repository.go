package repository

import (
	"aristio-test-axiata/domain/posts/model"
	"aristio-test-axiata/infrastructure/database"
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
)

type PostsRepository interface {
	CreatePostRepository(tx *sqlx.Tx, request model.PostsRequest) (rtx *sqlx.Tx, resp int64, err error)
	CreateTagRepository(tx *sqlx.Tx, request model.TagRequest) (rtx *sqlx.Tx, err error)
	UpdatePostRepository(tx *sqlx.Tx, request model.PostsRequest) (rtx *sqlx.Tx, err error)
	DeletePostRepository(tx *sqlx.Tx, postID int64) (rtx *sqlx.Tx, err error)
	DeleteTagsRepository(tx *sqlx.Tx, postID int64) (rtx *sqlx.Tx, err error)
	GetPostRepository(postID int64) (result *model.PostsResponse, err error)
	GetTagRepository(postID int64) (result *[]model.TagResponse, err error)

	StartTxRepository() (tx *sqlx.Tx, err error)
	RollbackTxRepository(tx *sqlx.Tx) (resTx *sqlx.Tx, err error)
	CommitTxRepository(tx *sqlx.Tx) (err error)
}

type postsRepository struct {
	db *database.Database
}

func NewPostsRepository(db *database.Database) PostsRepository {
	return &postsRepository{
		db: db,
	}
}

func (pr postsRepository) CreatePostRepository(tx *sqlx.Tx, request model.PostsRequest) (rtx *sqlx.Tx, resp int64, err error) {
	queryDelete := "INSERT INTO tbl_posts (title, content) VALUES ($1,$2) RETURNING id"
	err = tx.QueryRow(queryDelete, request.Title, request.Content).Scan(&resp)
	if err != nil {
		rtx = tx
		return
	}

	rtx = tx
	return
}

func (pr postsRepository) CreateTagRepository(tx *sqlx.Tx, request model.TagRequest) (rtx *sqlx.Tx, err error) {
	ctx := context.Background()
	queryInsert := "INSERT INTO tbl_tags (id_post, tag) VALUES ($1,$2)"
	_, err = pr.db.ExecContext(ctx, queryInsert, request.IDPost, request.Tag)
	if err != nil {
		rtx = tx
		return
	}
	rtx = tx
	return
}

func (pr postsRepository) UpdatePostRepository(tx *sqlx.Tx, request model.PostsRequest) (rtx *sqlx.Tx, err error) {
	ctx := context.Background()
	queryUpdate := "UPDATE tbl_posts SET title = $1, content = $2 WHERE id = $3"
	_, err = pr.db.ExecContext(ctx, queryUpdate, request.Title, request.Content, request.ID)
	if err != nil {
		rtx = tx
		return
	}
	rtx = tx
	return
}

func (pr postsRepository) DeletePostRepository(tx *sqlx.Tx, postID int64) (rtx *sqlx.Tx, err error) {
	ctx := context.Background()
	queryDelete := "DELETE FROM tbl_posts WHERE id = $1"
	_, err = pr.db.ExecContext(ctx, queryDelete, postID)
	if err != nil {
		rtx = tx
		return
	}
	rtx = tx
	return
}

func (pr postsRepository) DeleteTagsRepository(tx *sqlx.Tx, postID int64) (rtx *sqlx.Tx, err error) {
	ctx := context.Background()
	queryDelete := "DELETE FROM tbl_tags WHERE id_post = $1"
	_, err = pr.db.ExecContext(ctx, queryDelete, postID)
	if err != nil {
		rtx = tx
		return
	}
	rtx = tx
	return
}

func (pr postsRepository) GetPostRepository(postID int64) (result *model.PostsResponse, err error) {
	posts := model.PostsResponse{}
	queryShipment := `
	SELECT 
		id,
		title,
		content
	FROM tbl_posts p
	WHERE p.id=$1`

	ctx := context.Background()
	err = pr.db.GetContext(ctx, &posts, queryShipment, postID)
	if err != nil {
		if err == sql.ErrNoRows {
			return
		}

		return
	}

	result = &posts
	return
}

func (pr postsRepository) GetTagRepository(postID int64) (result *[]model.TagResponse, err error) {
	tags := []model.TagResponse{}
	queryTag := `
	SELECT 
		id_post,
		tag
	FROM tbl_tags t
	WHERE t.id_post=$1`

	ctx := context.Background()
	err = pr.db.SelectContext(ctx, &tags, queryTag, postID)
	if err != nil {
		if err == sql.ErrNoRows {
			return
		}

		return
	}

	result = &tags
	return
}

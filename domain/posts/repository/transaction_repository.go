package repository

import (
	"context"

	"github.com/jmoiron/sqlx"
)

func (r postsRepository) StartTxRepository() (tx *sqlx.Tx, err error) {
	tx, err = r.db.Beginx()
	if err != nil {
		return
	}

	return
}

func (r postsRepository) RollbackTxRepository(tx *sqlx.Tx) (resTx *sqlx.Tx, err error) {
	errRB := tx.Rollback()
	if errRB != nil {
		err = errRB
		return
	}

	resTx = tx
	return
}
func (r postsRepository) CommitTxRepository(tx *sqlx.Tx) (err error) {

	err = tx.Commit()
	if err != nil {
		errRB := tx.Rollback()
		if errRB != nil {
			err = errRB
			return
		}

		if err == context.DeadlineExceeded {
			return
		}

		return
	}
	return
}

package model

type PostsResponse struct {
	ID      int64    `db:"id" json:"id"`
	Title   string   `db:"title" json:"title"`
	Content string   `db:"content" json:"content"`
	Tags    []string `json:"tags"`
}

type PostsRequest struct {
	ID      int64        `db:"id" json:"id"`
	Title   string       `db:"title" json:"title"`
	Content string       `db:"content" json:"content"`
	Tags    []string     `json:"tags"`
	TagsReq []TagRequest `json:"tags_req"`
}

type TagResponse struct {
	IDPost int64  `db:"id_post" json:"id_post"`
	Tag    string `db:"tag" json:"tag"`
}

type TagRequest struct {
	IDPost int64  `db:"id_post" json:"id_post"`
	Tag    string `db:"tag" json:"tag"`
}

package feature

import (
	"aristio-test-axiata/domain/posts/model"
	"aristio-test-axiata/domain/posts/repository"
	"errors"
)

type PostFeature interface {
	CreatePostFeature(request model.PostsRequest) (response model.PostsResponse, err error)
	UpdatePostFeature(request model.PostsRequest) (response model.PostsResponse, err error)
	DeletePostFeature(postID int64) (err error)
	GetPostFeature(postID int64) (response model.PostsResponse, err error)
}

type postFeature struct {
	postsRepository repository.PostsRepository
}

func NewPostsFeature(postsRepository repository.PostsRepository) PostFeature {
	return &postFeature{
		postsRepository: postsRepository,
	}
}

func (pF *postFeature) CreatePostFeature(request model.PostsRequest) (response model.PostsResponse, err error) {
	tx, err := pF.postsRepository.StartTxRepository()
	if err != nil {
		return
	}
	rtx, resp, err := pF.postsRepository.CreatePostRepository(tx, request)
	if err != nil {
		_, errRb := pF.postsRepository.RollbackTxRepository(rtx)
		if errRb != nil {
			return
		}
		return
	}

	for _, tag := range request.Tags {
		reqTag := model.TagRequest{
			IDPost: resp,
			Tag:    tag,
		}
		rtx, errCreate := pF.postsRepository.CreateTagRepository(rtx, reqTag)
		if errCreate != nil {
			_, errRb := pF.postsRepository.RollbackTxRepository(rtx)
			if errRb != nil {
				return
			}
			return
		}
	}

	errCommit := pF.postsRepository.CommitTxRepository(rtx)
	if errCommit != nil {
		_, errRb := pF.postsRepository.RollbackTxRepository(rtx)
		if errRb != nil {
			return
		}
		return
	}

	response = model.PostsResponse{
		ID:      resp,
		Title:   request.Title,
		Content: request.Content,
		Tags:    request.Tags,
	}

	return
}
func (pF *postFeature) UpdatePostFeature(request model.PostsRequest) (response model.PostsResponse, err error) {
	tx, err := pF.postsRepository.StartTxRepository()
	if err != nil {
		return
	}
	rtx, err := pF.postsRepository.UpdatePostRepository(tx, request)
	if err != nil {
		_, errRb := pF.postsRepository.RollbackTxRepository(rtx)
		if errRb != nil {
			return
		}
		return
	}

	rtx, err = pF.postsRepository.DeleteTagsRepository(rtx, request.ID)
	if err != nil {
		_, errRb := pF.postsRepository.RollbackTxRepository(rtx)
		if errRb != nil {
			return
		}
		return
	}

	for _, tag := range request.Tags {
		reqTag := model.TagRequest{
			IDPost: request.ID,
			Tag:    tag,
		}
		rtx, err = pF.postsRepository.CreateTagRepository(rtx, reqTag)
		if err != nil {
			_, errRb := pF.postsRepository.RollbackTxRepository(rtx)
			if errRb != nil {
				return
			}
			return
		}
	}

	errCommit := pF.postsRepository.CommitTxRepository(rtx)
	if errCommit != nil {
		_, errRb := pF.postsRepository.RollbackTxRepository(rtx)
		if errRb != nil {
			return
		}
		return
	}

	response = model.PostsResponse{
		ID:      request.ID,
		Title:   request.Title,
		Content: request.Content,
		Tags:    request.Tags,
	}
	return
}
func (pF *postFeature) DeletePostFeature(postID int64) (err error) {
	tx, err := pF.postsRepository.StartTxRepository()
	if err != nil {
		_, errRb := pF.postsRepository.RollbackTxRepository(tx)
		if errRb != nil {
			return
		}
		return
	}
	rtx, err := pF.postsRepository.DeleteTagsRepository(tx, postID)
	if err != nil {
		_, errRb := pF.postsRepository.RollbackTxRepository(rtx)
		if errRb != nil {
			return
		}
		return
	}
	rtx, err = pF.postsRepository.DeletePostRepository(rtx, postID)
	if err != nil {
		_, errRb := pF.postsRepository.RollbackTxRepository(rtx)
		if errRb != nil {
			return
		}
		return
	}
	errCommit := pF.postsRepository.CommitTxRepository(rtx)
	if errCommit != nil {
		_, errRb := pF.postsRepository.RollbackTxRepository(rtx)
		if errRb != nil {
			return
		}
		return
	}
	return
}
func (pF *postFeature) GetPostFeature(postID int64) (response model.PostsResponse, err error) {
	posts, err := pF.postsRepository.GetPostRepository(postID)
	if err != nil {
		return
	}
	if posts == nil {
		err = errors.New("posts not found")
		return
	}
	tagsData := []string{}
	tags, err := pF.postsRepository.GetTagRepository(postID)
	if err != nil {
		return
	}

	if tags == nil {
		err = errors.New("tags not found")
		return
	}

	for _, tag := range *tags {
		tagsData = append(tagsData, tag.Tag)
	}

	posts.Tags = tagsData
	response = *posts
	return
}

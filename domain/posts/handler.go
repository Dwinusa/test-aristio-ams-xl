package posts

import (
	"aristio-test-axiata/domain/posts/feature"
	"aristio-test-axiata/domain/posts/model"
	"encoding/json"
	"net/http"
)

type PostsHandler interface {
	CreatePostHandler(w http.ResponseWriter, r *http.Request) error
	UpdatePostHandler(w http.ResponseWriter, r *http.Request, postID int64) error
	DeletePostHandler(w http.ResponseWriter, r *http.Request, postID int64) error
	GetPostHandler(w http.ResponseWriter, r *http.Request, postID int64) error
}

type postsHandler struct {
	postsFeature feature.PostFeature
}

func NewPostHandler(postsFeature feature.PostFeature) PostsHandler {
	return &postsHandler{
		postsFeature: postsFeature,
	}
}

func (h *postsHandler) CreatePostHandler(w http.ResponseWriter, r *http.Request) error {
	// Implementation
	var post model.PostsRequest
	err := json.NewDecoder(r.Body).Decode(&post)
	if err != nil {
		http.Error(w, "created failed decode", http.StatusNotFound)
		return err
	}
	res, err := h.postsFeature.CreatePostFeature(post)
	if err != nil {
		http.Error(w, "created failed", http.StatusNotFound)
		return err
	}

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(res)

	return nil
}

func (h *postsHandler) UpdatePostHandler(w http.ResponseWriter, r *http.Request, postID int64) error {
	var post model.PostsRequest
	err := json.NewDecoder(r.Body).Decode(&post)
	if err != nil {
		http.Error(w, "update failed decode", http.StatusBadRequest)
		return err
	}
	post.ID = postID
	res, err := h.postsFeature.UpdatePostFeature(post)
	if err != nil {
		http.Error(w, "update failed", http.StatusBadRequest)
		return err
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(res)
	// Implementation
	return nil
}

func (h *postsHandler) DeletePostHandler(w http.ResponseWriter, r *http.Request, postID int64) error {
	err := h.postsFeature.DeletePostFeature(postID)
	if err != nil {
		http.Error(w, "delete failed", http.StatusNotFound)
		return err
	}
	w.WriteHeader(http.StatusOK)
	// Implementation
	return nil
}

func (h *postsHandler) GetPostHandler(w http.ResponseWriter, r *http.Request, postID int64) error {
	res, err := h.postsFeature.GetPostFeature(postID)
	if err != nil {
		http.Error(w, "no data", http.StatusBadRequest)
		return err
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(res)
	// Implementation
	return nil
}

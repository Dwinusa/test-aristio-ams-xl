package server

import (
	"aristio-test-axiata/delivery/container"
	"aristio-test-axiata/domain/posts"
)

type handler struct {
	postHandler posts.PostsHandler
}

func SetupHandler(cont *container.Container) handler {
	return handler{
		postHandler: posts.NewPostHandler(cont.PostsFeature),
	}
}

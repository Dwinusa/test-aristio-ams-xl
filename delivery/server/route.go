package server

import (
	"net/http"
	"strconv"
	"strings"
)

func (h handler) handlePosts(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		h.postHandler.CreatePostHandler(w, r)
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func (h handler) handlePostByID(w http.ResponseWriter, r *http.Request) {
	urlParts := strings.Split(r.URL.Path, "/")
	if len(urlParts) != 4 {
		http.Error(w, "Invalid URL", http.StatusBadRequest)
		return
	}

	postID, err := strconv.Atoi(urlParts[3])
	if err != nil {
		http.Error(w, "Invalid post ID", http.StatusBadRequest)
		return
	}

	switch r.Method {
	case http.MethodGet:
		h.postHandler.GetPostHandler(w, r, int64(postID))
	case http.MethodPut:
		h.postHandler.UpdatePostHandler(w, r, int64(postID))
	case http.MethodDelete:
		h.postHandler.DeletePostHandler(w, r, int64(postID))
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

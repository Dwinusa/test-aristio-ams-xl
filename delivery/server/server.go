package server

import (
	"aristio-test-axiata/delivery/container"
	"encoding/json"
	"fmt"
	"net/http"
)

func ServeHttp(cont *container.Container) *http.ServeMux {
	fmt.Println("Starting http service...")

	h := SetupHandler(cont)

	// Initialize a new ServeMux
	mux := http.NewServeMux()

	// Define a custom error handler
	errorHandler := func(w http.ResponseWriter, r *http.Request, err error) {

		// Return an error response
		resp := map[string]interface{}{
			"status":  fmt.Sprintf("route %s or method not allowed", http.StatusText(http.StatusNotFound)),
			"message": fmt.Sprintf("route %s", err.Error()),
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(resp)
	}

	// Attach the custom error handler to a middleware
	mux.HandleFunc("/api/posts", func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				errorHandler(w, r, err.(error))
			}
		}()

		// Call your actual handler here
		h.handlePosts(w, r)
	})

	mux.HandleFunc("/api/posts/", func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				errorHandler(w, r, err.(error))
			}
		}()

		// Call your actual handler here
		h.handlePostByID(w, r)
	})

	return mux
}

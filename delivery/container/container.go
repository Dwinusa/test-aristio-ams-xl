package container

import (
	"fmt"
	"log"

	"aristio-test-axiata/config"
	posts_feature "aristio-test-axiata/domain/posts/feature"
	posts_repository "aristio-test-axiata/domain/posts/repository"
	"aristio-test-axiata/infrastructure/database"
)

type Container struct {
	EnvironmentConfig config.EnvironmentConfig
	PostsFeature      posts_feature.PostFeature
}

func SetupContainer() Container {
	cfg, err := config.LoadENVConfig()
	if err != nil {
		log.Panic(err)
	}

	fmt.Println("Loading database...")
	db, err := database.LoadPsqlDatabase(cfg.Database)
	if err != nil {
		log.Panic(err)
	}

	fmt.Println("Loading repository's...")
	postsRepository := posts_repository.NewPostsRepository(db)

	postsFeature := posts_feature.NewPostsFeature(postsRepository)

	return Container{
		EnvironmentConfig: cfg,
		PostsFeature:      postsFeature,
	}
}

package database

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func LoadPsqlDatabase(config DatabaseConfig) (database *Database, err error) {
	datasource := fmt.Sprintf("user=%s dbname=%s password=%s host=%s sslmode=disable",
		// datasource := fmt.Sprintf("%s://%s:%s@%s/%s?sslmode=disable",
		// config.Dialect,
		config.Username,
		config.Name,
		config.Password,
		config.Host,
	)
	db, err := sqlx.Connect(config.Dialect, datasource)
	if err != nil {
		err = fmt.Errorf("error connection to db %s", err)
		return
	}

	database = &Database{
		db,
	}

	return
}

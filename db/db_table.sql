-- public.tbl_posts definition

-- Drop table

-- DROP TABLE public.tbl_posts;

CREATE TABLE public.tbl_posts (
	id bigserial NOT NULL,
	title varchar(40) NOT NULL,
	"content" text NULL,
	CONSTRAINT tbl_posts_pk PRIMARY KEY (id)
);

-- public.tbl_tags definition

-- Drop table

-- DROP TABLE public.tbl_tags;

CREATE TABLE public.tbl_tags (
	id bigserial NOT NULL,
	id_post int8 NOT NULL,
	tag varchar(40) NULL,
	CONSTRAINT tbl_tags_pk PRIMARY KEY (id)
);
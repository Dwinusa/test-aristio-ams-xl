package cmd

import (
	"aristio-test-axiata/delivery/container"
	"aristio-test-axiata/delivery/server"
	"fmt"
	"net/http"
)

func Execute() {
	container := container.SetupContainer()
	mux := server.ServeHttp(&container)

	// Create a new HTTP server with the provided ServeMux
	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", container.EnvironmentConfig.App.Port), // Specify the port you want to listen on
		Handler: mux,                                                      // Use the ServeMux returned from ServeHttp
	}

	// Start the HTTP server
	fmt.Printf("App %s starting  \n", container.EnvironmentConfig.App.Name)
	fmt.Printf("App version : %s \n", container.EnvironmentConfig.App.Version)
	fmt.Printf("Server listening on port %d \n", container.EnvironmentConfig.App.Port)
	err := server.ListenAndServe()
	if err != nil {
		fmt.Printf("Error starting server: %s\n", err)
	}

}

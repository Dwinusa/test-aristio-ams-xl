# test-aristio-ams-xl

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## library use

For action library install : go mod tidy
- github.com/jmoiron/sqlx
- github.com/joho/godotenv
- github.com/lib/pq

## Database

Please generate query from create table in folder db

## Fill env 

Fill env for run program

## CURL FOR CREATE

curl --request POST \
  --url http://localhost:3000/api/posts \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/9.2.0' \
  --data '{ 
"title": "REST API with Go", 
"content": "Lorem ipsum", 
"tags": ["Go", "API"] 
}'

## CURL FOR UPDATE

curl --request PUT \
  --url http://localhost:3000/api/posts/3 \
  --header 'Content-Type: application/json' \
  --header 'User-Agent: insomnia/9.2.0' \
  --data '{ 
"title": "REST API with Go 2", 
"content": "Lorem ipsum", 
"tags": ["Go2", "API"] 
}'

## CURL FOR DELETE

curl --request DELETE \
  --url http://localhost:3000/api/posts/2 \
  --header 'User-Agent: insomnia/9.2.0'

## CURL FOR GET

curl --request GET \
  --url http://localhost:3000/api/posts/3 \
  --header 'User-Agent: insomnia/9.2.0'
